package com.example.workreport.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.workreport.Work
import java.util.*

@Dao
interface WorkDao{
    @Query("SELECT * FROM work")
    fun getJobs(): LiveData<List<Work>>

    @Query("SELECT * FROM work WHERE id=(:id)")
    fun getJob(id: UUID) : LiveData<Work>

    @Update
    fun updateJob(work: Work)

    @Insert
    fun addJob(work: Work)
}