package com.example.workreport

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.example.workreport.database.*
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.Executors

private const val DATABASE_NAME = "work-database"

class WorkRepository private constructor(context: Context){

    private val database: WorkDatabase = Room.databaseBuilder(
            context.applicationContext,
            WorkDatabase::class.java,
            DATABASE_NAME
    ).build()
    private val workDao = database.workDao()
    private val executor = Executors.newSingleThreadExecutor()

    fun getJobs(): LiveData<List<Work>> = workDao.getJobs()
    fun getJob(id: UUID): LiveData<Work>? = workDao.getJob(id)

    fun updateJob(work: Work){
        executor.execute{
            workDao.updateJob(work)
        }
    }

    fun addJob(work: Work){
        executor.execute{
            workDao.addJob(work)
        }
    }

    companion object{
        private var INSTANCE: WorkRepository? = null

        fun initialize(context: Context){
            if(INSTANCE == null){
                INSTANCE = WorkRepository(context)
            }
        }
        fun get(): WorkRepository{
            return INSTANCE ?: throw IllegalStateException("WorkRepository must be initialized!")
        }
    }
}