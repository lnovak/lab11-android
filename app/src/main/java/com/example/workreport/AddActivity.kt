package com.example.workreport

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.example.workreport.WorkRepository
import kotlinx.android.synthetic.main.activity_add.*

class AddActivity : AppCompatActivity() {
    private lateinit var work: Work
    private lateinit var titleField: EditText
    private lateinit var dateButton: Button
    private lateinit var doneCheckBox: CheckBox
    private lateinit var btnSave: Button
    private val workRepository = WorkRepository.get()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        work = Work()

        titleField = findViewById(R.id.work_title)
        dateButton = findViewById(R.id.work_date)
        doneCheckBox = findViewById(R.id.work_done)
        btnSave=findViewById(R.id.btnSave)

        dateButton.apply {
            text = work.date.toString()
            isEnabled = false
        }

        doneCheckBox.apply{
            setOnCheckedChangeListener{_, isChecked ->
                work.isDone = isChecked
            }
        }

        btnSave.setOnClickListener {
            createJob()
            Toast.makeText(this,"Work report created",Toast.LENGTH_SHORT).show()
        }

        btnCancel.setOnClickListener{
            finish()
        }
    }

    private fun createJob(){
        work.title=titleField.text.toString()
        workRepository.addJob(work)
    }
}