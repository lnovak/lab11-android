package com.example.workreport

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class StartActivity : AppCompatActivity() {
    private lateinit var btnAdd: Button
    private lateinit var btnAbout: Button
    private lateinit var btnReports: Button

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        btnAdd = findViewById(R.id.btnAddReport)
        btnAbout = findViewById(R.id.btnAbout)
        btnReports = findViewById(R.id.btnReports)

        btnAdd.setOnClickListener{
            val intent = Intent(this, AddActivity::class.java)
            startActivity(intent)
        }

        btnAbout.setOnClickListener{
            // implement
        }

        btnReports.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}